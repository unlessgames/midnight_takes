midi = 
  active : false
  walls : [0..7].map((i) -> 0)
  init : () ->
    if not isMobile.any
      if settings.audio is "midi"
        WebMidi.enable((err) -> if not err then midi.setup() else settings.audio = "tone")
    else
      settings.audio = "tone"
  setup : () ->
    @active = true
    @inputs = WebMidi.inputs
    @outputs = WebMidi.outputs
    @output = 0
    console.log(@outputs.map((o) -> o.name))
  send_cc : (n, v) ->
    @outputs[@output].sendControlChange(n, v)
  # keys : []
  send_note : (n) ->
    @outputs[@output].playNote n, "all", {duration : 100}
  
  process : (inputs) ->
    for w, i in inputs.walls
      nt = 127 - constrain(map(w.distance, 0, 300, 0, 127), 0, 127)
      if @walls[i] != nt
        @walls[i] = nt
        @send_cc i, @walls[i]
        # console.log "sending cc to #{i}"

    for a, i in inputs.ambients
      @send_cc 8 + i, Math.round(a * 127)

    if inputs.destination?
      @send_note inputs.destination


