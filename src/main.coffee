Outputs = 
  process : (inputs) ->
    if settings.audio is "midi" and midi.active
      midi.process inputs
    else
      toner.process inputs

scenes = 
  editor : editor
  renderer : renderer


editing = false

current = "editor"

max_level = 2

the_end = false

page = 0

texter = 
  setup : (font) ->
    @screen = createGraphics width, height
    @screen.textFont font
  eye : (p, w, h) ->
    @screen.noFill()
    @screen.stroke(200)
    @screen.beginShape()
    @screen.curveVertex(p.x - w, p.y)
    @screen.curveVertex(p.x - w * 0.5, p.y + h * 0.5)
    @screen.curveVertex(p.x, p.y + h)
    @screen.curveVertex(p.x + w * 0.5, p.y + h * 0.5)
    @screen.curveVertex(p.x + w, p.y)
    # @screen.curveVertex(p.x - w,p.y)
    # @screen.curveVertex(p.x,p.y + h)
    # @screen.curveVertex(p.x + w,p.y)
    @screen.endShape()
    @screen.ellipse p.x, p.y, h * 2

  write : (t) ->
    lines = t.split("\n").length
    fs = 48
    @screen.fill("#00000066")
    @screen.noStroke()
    @screen.rect(0, 0, @screen.width, @screen.height)
    @screen.textAlign CENTER
    @screen.textSize(fs)
    @screen.noSmooth()
    @screen.fill(200)
    @screen.text(t, width * 0.5, height * 0.5 - Math.floor(lines / 2) * fs)

    # @eye createVector(@screen.width / 2, @screen.height / 2), width * 0.5, width * 0.1
    # @eye createVector(@screen.width / 2, @screen.height / 2), width * 0.5, -width * 0.1
    noStroke()
    texture(@screen)
    plane(window.innerWidth, window.innerHeight)
  draw : () ->



class MenuPage
  constructor : (@setup, @draw, @exit) ->

menu = 
  current : 0
  pages : [
    new MenuPage(
      () ->
      () -> texter.write("midnight takes")
      () ->
    )
    new MenuPage(
      () ->
      () -> 
        t = 
          if isMobile.any then "turn to turn\ntap to move"
          else "space moves\narrows turn"
        texter.write(t)
      () ->
    )
  ]
  next_page : () -> 
    @current += 1
    @current < @pages.length
  draw : () ->
    if @current < @pages.length
      @pages[@current].draw()




maps = null


terminus = null

preload = () ->
  terminus = loadFont("lib/Terminus.ttf")
  maps = loadJSON "maps.json"


set_projection = () ->
  ortho(-width / 2, width / 2, -height / 2, height / 2, 0, 500);

windowResized = () ->
  resizeCanvas(window.innerWidth, window.innerHeight)
  set_projection()

setup = () ->

  createCanvas window.innerWidth, window.innerHeight, WEBGL

  set_projection()
  # window.onresize = resize
  # textFont terminus
  
  level.load maps[page]


  Object.keys(scenes).forEach (k) -> scenes[k].setup(maps)


  toner.setup()
  midi.init()

  texter.setup(terminus)

last_time = new Date().getTime()

print_gyro = (g) -> "#{g.x} : #{g.y} : #{g.z}"

draw = () ->
  if toner.has_all_buffers
    _now = new Date().getTime()
    deltaTime = last_time - _now
    last_time = _now

    Inputs.update()

    clear()
    # noSmooth()
    noStroke()
    fill 0
    # rect 0, 0, width, height 
    
    
    level_out = level.process Inputs

    Outputs.process level_out

    level.player.draw_rays level_out.walls


    if editing
      editor.draw()
    else
      # push()
      renderer.draw level_out
      # pop()


  # if menu.current < menu.pages.length
  #   menu.draw()




  # if current is "renderer" and renderer.complete and not the_end
  #   if editor.page < max_level - 1
  #     renderer.complete = false
  #     # next_level()
  #   else
  #     console.log "THE END"
  #     the_end = true

  # if the_end
    # text "THE END", width * 0.5, height * 0.5

  # noStroke()
  # textSize 18
  # text gyro.print(), width * 0.5, height * 0.5
  # text settings.print(), width - 40, 20


keyPressed = () ->
  # console.log keyCode
  toner.try_start_audio()
  menu.next_page()

  if keyCode is 69 # E
    editing = !editing

  if editing then editor.keypress()

keyReleased = () ->
  if editing then editor.keyup()




mousePressed = () ->
  toner.try_start_audio()
  menu.next_page()
  if editing then editor.mousepress()

mouseWheel = (e) ->
  if editing then editor.scroll e

save_levels = (l) ->
  console.log "saving level"
  maps[page] = level.save()
  socket.emit("save", name : "maps.json", data : maps)



###
TODO

toner
  play ambient
  play destination

indicate moving
  sound
  vibrate

?show level complete

menu
  headphones ? -> resume audio

  controls
    arrows
    turning with screen
    ?midi
    ?controller

tutorial
  close eyes bell
  open eyes bell



level select
  save/load progress

intro
  midnight takes
  headphones?
  screen or gyro calibrate
  move
  bell?
  close eyes


###


