zone_types = ["wall", "ambient", "destination"]    

class Zone
  active : true
  constructor : (@type, @pos, @radius, @note) ->
  @load : (z) ->
    new Zone(z.type, createVector(parseFloat(z.x), parseFloat(z.y)), z.radius, z.note)
  save : () ->
    type : @type
    x : @pos.x
    y : @pos.y
    radius : @radius
    note : @note
  get_volume : (p) ->
    map(dist(p.x, p.y, @pos.x, @pos.y), 0, @radius, 1, 0)
  color : (t, i) ->
    switch t
      when "wall"
        40
      when "ambient"
        colorMode(HSL)
        c = color((i / 9) * 360, 40, 50, 0.8)
        alpha c
        c
      when "destination"
        colorMode(HSL)
        color (i / 9) * 360, 70, 50
  contains : (p) ->
    dist(@pos.x, @pos.y, p.x, p.y) < @radius
    
  draw : (stroked) ->
    if @active
      if stroked
        stroke 200
      else
        noStroke()
      fill @color @type, @note
      # fill "hsla(0, 0.5, 0.5)" #@color @type, @note.id
      ellipse @pos.x, @pos.y, @radius * 2

class Note
  constructor : (@id) ->

class Level
  zones : []
  player : new Player()
  destinations : 0
  id : 0
  constructor : (i = 0, z) ->
    @id = i
    if z? then @load z
  add_zone : (t, p, r, n) ->
    z = new Zone t, p, r, n
    @zones.push z
    z

  inside : (zone, p) ->
    zone.contains p
  collide : (p, target, max_count = 1) ->
    r = []
    for z in @zones.filter((z) -> (!target? || z.type is target) && z.active)
      if z.contains p
        r.push z
      if r.length is max_count
        break
    r

  find_closest : (p) ->
    @zones.reduce(((a, zone, i) => 
      if zone.contains(p) and (a is -1 or p.dist(@zones[a].pos) > p.dist(zone.pos))
        i
      else
        a
    ), -1
    )
  get_closest : (p) ->
    id = @find_closest p
    if id isnt -1
      @zones[id]
    else null
  delete : (p) ->
    id = @find_closest p
    if id isnt -1
      @zones.splice(id, 1)

  inside_level : (p) -> 
    p < @player.view_distance
    # p.x > 0 and p.x < @bounds.w and p.y > 0 and p.y < @bounds.h
  process : (inputs) ->
    @player.update(@wall_force(@player.pos), inputs)
    
    steps = 7
    half = Math.floor(steps / 2)
    fov = Math.PI * 0.6
    p = @player
    
    ambients = @collide(@player.pos, "ambient")
    destination = @collide(@player.pos, "destination")
    destination = 
      if destination.length > 0
        id = @zones.findIndex((z) -> z.type is "destination" and z.note is destination[0].note)
        @zones[id].active = false
        @destinations--
        id
      else undefined


    outputs = 
      walls : [-half..half].map((pan, i) => 
        t = i / (steps - 1)
        r = @raycast(
          @player.pos
          p5.Vector.fromAngle(@player.dir + ((1 - t) - 0.5) * fov), @player.view_distance
        )
        r.pan = pan
        r
      )
      ambients : [8..15].map(
        (note, i) => 
          z = ambients.find((z) -> z.note.id is note)
          if z?
            z.get_volume(@player.pos)
          else 
            0
      )
      destination : destination
      win : @destinations is 0
    outputs

  raycast : (p, d, max_dist) ->
    out_of_level = false
    collision = null
    step = 1
    v = p.copy()
    while not out_of_level and !collision?
      if not @inside_level dist(v.x, v.y, p.x, p.y)
        out_of_level = true
      else
        ls = @collide v, "wall"
        if ls.length > 0
          collision = ls[0]
        else
          v.add d.mult(step)
    zone : collision
    point : v
    distance : dist v.x, v.y, p.x, p.y
  wall_force : (p) ->
    ws = @collide p, "wall"
    r = createVector(0,0)
    for w in ws
      r.add(p5.Vector.sub(p, w.pos).normalize())
    r
  load : (ls) ->
    l = ls
    @zones = []
    @id = l.id
    for z in l.zones
      @zones.push Zone.load(z)
    @player = Player.load(l.player)
    @destinations = @zones.reduce(((a, v) -> if v.type is "destination" then a + 1 else a), 0)

    console.log "map = ", @id, @zones, @player, @destinations
    @

  save : () ->
    zones : @zones.map((z) -> z.save())
    player : @player.save()
    id : @id

  center_rect : (x, y, w, h) ->
    rect(x - w * 0.5, y - h * 0.5, w, h)

  draw : (stroke) ->
    p = @player.pos
    camera(p.x, p.y, 1, p.x, p.y, 0, 0,1,0)

    @zones.forEach((zone) -> zone.draw(stroke))
    @player.draw()

    # stroke(255)
    # noFill()    
    # text "#{@destinations}", width - 40, height - 20


level = new Level()
