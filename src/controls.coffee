gyro = 
  activated : false
  data : null
  error : ""
  activate : (dt, b ) ->
    gyro.activated = b
    gyro.data = dt
  print : () ->
    if gyro.activated
      "#{gyro.data.do.alpha} \n#{gyro.data.do.beta} \n#{gyro.data.do.gamma}"
    else 
      gyro.error

if isMobile.any 
  gn = new GyroNorm()
  gn.init().then(
    ()->
      gn.start((data) -> gyro.activate(data, isMobile.any))).catch((e) -> settings.control = "screen")
else
  settings.control = "keys"

class Input 
  move : 0
  turn : 0
  absolute : false
  update : () ->
    @absolute = isMobile.any && gyro.activated
    @move = 
      if mouseIsPressed or keyIsDown(32) or keyIsDown 38 then 1
      else 0
    @turn = 
      if @absolute
        -1 * radians gyro.data.do.alpha
      else
        if keyIsDown(65) or keyIsDown 37 then -1
        else if keyIsDown(68) or keyIsDown 39 then 1
        else 0
  constructor : () ->


Inputs = new Input()
