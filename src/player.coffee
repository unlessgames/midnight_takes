deltaTime = 16

class Player
  turn_speed : 1
  move_speed : 50
  view_distance : 400
  constructor : (@pos, @dir) ->
  dir_vector : (m = 1) ->
    p5.Vector.fromAngle(@dir).mult(m)
  move : () ->
    @moving = true
    @pos.add @dir_vector(@move_speed * deltaTime * 0.001)
  turn : (r) ->
    if r
      @turning = r

    @dir += @turning * @turn_speed * deltaTime * 0.001
    # @pos.x += @dir.x
    # @pos.y += @dir.y
  @load : (p) ->
    p = new Player(createVector(p.x, p.y), p.dir)
    p

  save : () ->
    x : @pos.x
    y : @pos.y
    dir : @dir
  stop_turn : () ->
    @turning = 0
  update : (wall_force, inputs) ->
    if inputs.move > 0 then @move()
    if inputs.absolute
      @dir = inputs.turn
    else
      if inputs.turn != 0 then @turn(inputs.turn)
    @pos.add(wall_force)

  draw_rays : (rays) ->
    stroke 180
    strokeWeight 1
    fill 0
    beginShape LINES
    for r in rays
      if r?
        vertex(@pos.x, @pos.y)
        vertex(r.point.x, r.point.y)
        stroke "#ffdd00"
        ellipse r.point.x, r.point.y, 10
    endShape()
    noStroke()

  draw : () ->
    w = 15
    fill 255
    noStroke()
    ellipse @pos.x, @pos.y, w

