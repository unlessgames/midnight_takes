editor =
  keycodes : [ 87, 65, 68 ]
  note : 0
  notes : [ new Note(0, '#ddddddff'), new Note(0, '#dd6666aa'), new Note(0, '#ddaa22ff')]
  radius : 30
  object : zone_types[0]
  hovered : []

  local : false

  removing : false
  gui : 
    setup : (font) ->
      @screen = createGraphics width, height
      @screen.textFont font
    write : (t) ->
      @screen.clear()
      lines = t.split("\n").length
      fs = 32
      # @screen.fill(0)
      # @screen.rect(0, 0, @screen.width, @screen.height)
      @screen.textAlign LEFT
      @screen.textSize(fs)
      @screen.noSmooth()
      @screen.fill(200)
      @screen.text(t, 0, height * 0.5 - Math.floor(lines / 2) * fs)

      noStroke()
      texture(@screen)
      plane(window.innerWidth, window.innerHeight)
    draw : () ->

  setup : (ms) ->
    # new Level().load(JSON.stringify(zones : [], player : new Player(createVector(0, 0)).save(), id : i))
    @default_cursor()
    @gui.setup terminus

  set_cursor : (t, x, y, r, n) ->
    @cursor = new Zone t, createVector(x, y), r, n
  default_cursor : () ->
    @set_cursor @object, mouseX, mouseY, @radius, 0

  mousepress : () ->
    level.add_zone @object, createVector(@cursor.pos.x, @cursor.pos.y), @radius, @cursor.note
  keypress : () ->
    if keyCode > 47 and keyCode < 58
      i = keyCode - 48
      if keyIsDown 16
        @cursor.note = i
        console.log "color = ", i

      else
        level.load maps[i]
    else
      switch keyCode
        when 76 # L
          if socket? then save_levels()
        when 82
          @removing = true
        when 80
          level.player = new Player( @cursor.pos.copy(), - Math.PI * 0.5)
        else
          k = @keycodes.reduce(((a, v, i) -> if v is keyCode then i else a), -1)
          if k isnt -1
            @object = zone_types[k]
            @default_cursor()
            console.log "setting cursor", @notes[zone_types.indexOf(@object)]
          
  keyup : () ->
    switch keyCode
      when 82
        @removing = false
        level.delete @cursor.pos
  scroll : (e) ->
    speed = 0.3
    @radius = constrain(@radius + e.delta * speed, 5, Math.min(width, height))
    @default_cursor()

  draw_gui : () ->
    fs = 24
    fill(255)
    # textSize fs
    x = 10
    y = 0
    underline = () ->
      line x, y, x + fs * 0.5 + 1, y
    for z, i in zone_types.concat ["save", "enter game", "remove"]
      y = fs + fs * i
      noStroke()
      fill(if z is @object then 255 else 180)
      # text z, x, y
      stroke 255 
      y += 4
      underline()

  draw : () ->
    # @draw_gui()
    level.draw(true)

    p = level.player.pos

    camera(p.x, p.y, 1, p.x, p.y, 0, 0,1,0)

    center = createVector width * 0.5, height * 0.5
    @cursor.pos.x = mouseX - center.x + p.x
    @cursor.pos.y = mouseY  - center.y+ p.y

    # translate center

    if @removing
      target = level.get_closest @cursor.pos
      if target?
        strokeWeight 2
        fill '#ff0000aa'
        noStroke()
        ellipse target.pos.x, target.pos.y, target.radius * 2
    else
      @cursor.draw()


    # @gui.write "n:#{@cursor.note}\n#{@cursor.type}\np:#{page}"
    # translate -center.x, -center.y

    # text @page, 40, height - 20

