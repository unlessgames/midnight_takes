master = Tone.Master

reverb = 
  new Tone.Freeverb().connect(master)

noise_source = new Tone.Noise("pink").start()

# ambient_model = {
#   frequency : "C3",
#   detune : 0 ,
#   oscillator : {
#   type : "square"
#   } ,
#   filter : {
#   Q : 6 ,
#   type : "lowpass" ,
#   rolloff : -24
#   } ,
#   envelope : {
#   attack : 0.005 ,
#   decay : 0.1 ,
#   sustain : 0.9 ,
#   release : 1
#   } ,
#   filterEnvelope : {
#   attack : 0.06 ,
#   decay : 0.2 ,
#   sustain : 0.5 ,
#   release : 2 ,
#   baseFrequency : 200 ,
#   octaves : 7 ,
#   exponent : 2
#   }
# }

class AmbientSynth
  constructor : (@source, @notes) ->
    @source.chain(Tone.Master)
    console.log @source
    @part = new Tone.Pattern(
      callback : (time, note) => 
        console.log time, note
        @source.triggerAttackRelease(note, "8n")
      pattern : Tone.CtrlPattern.Type.Up
      values : @notes.map((n) -> n[1])
    )


class SpatialNode
  constructor : (@source, @id, @pan) ->
    @filter = new Tone.Filter(100, "bandpass")
    @vol = new Tone.Volume(-128)
    @panner = new Tone.Panner3D(panningModel : "HRTF", distanceModel : "exponential")
    # @vol = new Tone.PanVol(@pan,)
    @source.chain(@vol, @filter, @panner, master)
    @distance = 1
    @dir = p5.Vector.fromAngle(@pan)
  distance_transform : (d) -> -0.6 * (Math.log(1 - d) + 1)
  set_filter : (d) ->
    nt = @distance_transform d
    if @distance != nt
      @distance = nt
      @filter.frequency.value = constrain(0 + 2000 * @distance, 0, 2000)
  set_position : (w) ->
    rp = p5.Vector.mult(@dir, w.distance * 0.1)
    @panner.setPosition(rp.x, 0, rp.y)


toner = 
  walls : []
  # ambients : ["./assets/terra-cotta.mp3", "./assets/um1.mp3"]

  loaded_buffer : (b, i) ->
    # @ambients[i] = b
    @loaded[i] = true
    if @loaded.reduce(((a, v, i) -> if not v then false else a), true)
      @has_all_buffers = true
  
  setup : () ->
    @has_all_buffers = true
    # @loaded = @ambients.map((a) -> false)
    # @ambients.map((a, i) =>
    #   Tone.Buffer.load(a, (b) => @loaded_buffer(b, i))
    # )

    Tone.Master.volume.value = -64
    @wallsynths = [-3..3].map((x,i) -> 
      t = i / 6
      new SpatialNode(noise_source, i, (t - 0.5) * -Math.PI * 0.6 + Math.PI * 0.5))
    # @ambientsynths = [
    #   new AmbientSynth(
    #     new Tone.MonoSynth(),
    #     # new Tone.MonoSynth(ambient_model).toMaster(),
    #     [["0:0:0", "C3"], ["0:0:2", "G3"], ["0:0:8", "F#4"], ["0:1:0", "B3"]]
    #   )
    # ]
  process : (inputs) ->
    reverb_size = 0
    p = level.player

    for w, i in inputs.walls
      ws = @wallsynths[i]
      nt = (127 - constrain(map(w.distance, 0, 300, 0, 127), 0, 127)) / 127
      ws.vol.volume.value = -12
      ws.set_filter nt
      ws.set_position w, p, nt
      reverb_size += nt
    # reverb_size /= inputs.walls.length
    # reverb.roomSize.value = 1 - reverb_size
    reverb.roomSize.value = 0.6

  try_start_audio : () ->
    if Tone.context.state isnt "running" || Tone.Master.volume.value != 0
      Tone.context.resume()
      Tone.Master.volume.rampTo(0, 3)
      # @ambientsynths[0].part.start(0)


    # for a, i in inputs.ambients
    #   @send_cc 8 + i, Math.round(a * 127)
