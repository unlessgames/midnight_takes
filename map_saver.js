const app = require("express")()
const http = require("http").createServer(app)
const io = require('socket.io')(http)

const fs = require('fs')

const filename = "data.json"

const port = process.env.PORT || 3000

function save_file(message) {
  let name = message.name ? message.name : filename
  fs.copyFile(name, name + '.backup', (err) => {
    if(err) throw err
    console.log(name + ' backed up')
  })
  let d = message.data ? message.data : message
  fs.writeFile(name, JSON.stringify(d, null, 2), (err) => {
    if(err) throw err
    console.log("wrote file to " + name)
  })
}

io.on('connection', (socket) => socket.on("save", save_file))
http.listen(port, () => console.log("server listening on :" + port))
